import { QuizStackPage } from './app.po';

describe('quiz-stack App', () => {
  let page: QuizStackPage;

  beforeEach(() => {
    page = new QuizStackPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
